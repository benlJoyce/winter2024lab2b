import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	//for loop that checks if the letter is in the word
	for (int i =0; i<word.length() ; i++){
	
		if (word.charAt(i)==c){
			return i;
		}
			
	}
	   return -1;
	}
	public static char toUpperCase(char c) {
		//Changes the current character to uppercase
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean[] letters) {
		String newWord = "";
		for (int i = 0; i < word.length() && i < letters.length; i++) {
			if (letters[i]) {
				newWord += Character.toUpperCase(word.charAt(i)) + " ";
			} else {
				newWord += " _ ";
			}
		}
		System.out.println("Your result is " + newWord);
	}

    public static void runGame(String word) {
        Scanner reader = new Scanner(System.in);
        boolean isWordGuessed = false;
        boolean[] lettersPosition = new boolean[word.length()];
        int attempts = 5;
        // Initialize lettersPosition array to false before guessing
        for (int i = 0; i < word.length(); i++) {
            lettersPosition[i] = false;
        }

        while (attempts >= 0 && !isWordGuessed) {
            isWordGuessed = true;
            //Ask for user input
            System.out.println("Guess a letter:");
            char guess = reader.nextLine().charAt(0);
            guess = toUpperCase(guess);

            // Checks if the guessed letter is in the word
            if (isLetterInWord(word, guess) != -1) {
                lettersPosition[isLetterInWord(word, guess)] = true;
            } else {
                System.out.println("Wrong guess :( Attempts left: " + attempts);
                attempts--;
            }

            // Display current progress
            printWork(word, lettersPosition);

            // Checks if word guessed
            for (int i = word.length()-1; i >= 0; i--) {
                isWordGuessed = isWordGuessed && lettersPosition[i];
            }
        }
        if (isWordGuessed) {
            System.out.println("Congrats! You got it :) The word was " + word);
        } else {
            System.out.println("Better luck next time! The word was " + word);
        }
    }
}