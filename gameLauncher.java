import java.util.Scanner;
public class gameLauncher{
	public static void main(String[] args){
		System.out.println("Hello! Welcome to game launcher");
		
		//ask user to play either hangman or wordle
		System.out.println("Enter 1 to play  Worlde or Enter 2 to play Hangman");
		Scanner scanner = new Scanner(System.in);
		int choice = scanner.nextInt();
		
		if(choice == 1){
			String word = wordle.generateWord();
			wordle.runGame(word);
		}
		else{
			System.out.println("Enter a 4-letter word:");
			String word = scanner.next();
			System.out.print("\033[H\033[2J");
			word = word.toUpperCase();
			Hangman.runGame(word);
		}
	}
}
