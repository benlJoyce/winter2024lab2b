import java.util.Scanner;
import java.util.Random;

public class wordle {

	public static String generateWord(){
		String[] words = new String[]{"ADOPT", "BRAKE", "BLING", "ALTER", "CHUNK", "FLARE", "STEAM", "WHIRL",
		"ABODE", "IRONY", "ZEBRA", "NIGHT", "HASTE", "HOVER", "WALTZ", "POISE", "GRAIL", "PHONY", "WAVER", "WHIRL", "QUAIL"};
		
		Random random = new Random();
		
		int randomIndex = random.nextInt(words.length);
		String generatedWord = words[randomIndex];
		
		return generatedWord;
	}
	
	public static boolean letterInWord(String word, char letter){
		boolean isInWord = false;
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == letter){
				isInWord = true;
			}
		}
		return isInWord;
	}
	
	public static boolean letterInSlot(String word, char letter, int position){
		if(position >= 0 && position < word.length()){
			return word.charAt(position) == letter;
		}
		return false;
	}
	
	public static String[] guessWord(String answer, String guess){
		String[] colours = new String[answer.length()];
		
		for(int i = 0; i < answer.length(); i++){
			if(answer.charAt(i) == guess.charAt(i)){
				colours[i] = "green";			
			}
			else if(letterInWord(answer, guess.charAt(i))){
				colours[i] = "yellow";
			}
			else{
				colours[i] = "white";
			}
		}
		return colours;
	}
	
	public static void presentResults(String word, String[] colours) {
		final String ANSI_RESET = "\u001B[0m";
		final String ANSI_YELLOW = "\u001B[33m";
		final String ANSI_GREEN = "\u001B[32m";
		final String ANSI_WHITE = "\u001B[37m";

		for (int i = 0; i < word.length(); i++) {
			String colour = colours[i];
			char currentLetter = word.charAt(i);

			switch (colour) {
				case "green":
					System.out.print(ANSI_GREEN + currentLetter + " " + ANSI_RESET);
					break;

				case "yellow":
					System.out.print(ANSI_YELLOW + currentLetter + " " + ANSI_RESET);
					break;

				case "white":
					System.out.print(ANSI_WHITE + currentLetter + " " + ANSI_RESET);
					break;
			}
		}
		System.out.println(); 
	}

	public static String readGuess(){
		
		Scanner scanner = new Scanner(System.in);
		String guess = "";
		
		while(guess.length() != 5){
			System.out.println("\nPlace a guess :D");
			guess = scanner.nextLine();
			
			if(guess.length() != 5){
				System.out.println("5 letters only >:( try again");
			}
		}
		return guess.toUpperCase();
	}
	
	public static void runGame(String word) {
		int guessCount = 0;
		boolean isGuessCorrect = false;

		while (guessCount < 6 && !isGuessCorrect) {
			String guess = readGuess();
			String[] colours = guessWord(word, guess);
			presentResults(guess, colours);

			isGuessCorrect = true; 

			for (int i = 0; i < colours.length; i++) {
				if (!(colours[i].equals("green"))) {
					isGuessCorrect = false;
				}
			}

			if (isGuessCorrect) {
				System.out.println("\nWOOHOO YOU WIN!!! CONGRATS");
			} else {
				guessCount++;

				if (guessCount < 6) {
					System.out.println("Getting close to an L.. you have " + (6 - guessCount) + " tries left");
				} 
				else {
                System.out.println("\nLOSERRRR YOU RAN OUT OF GUESSES, better luck next time buddy. The word was: " + word);
				}
			}
		}
	}
}

